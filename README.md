# "THE NEW YORK TIME" IOS EXAMPLE APP #

This simple app was developed to show an example of Swift.

### FEATURES ###
* This app connects to the API of "The New York Times" to show the most popular news of the last 30 days.
* It is fully responsive
* Uses 3rd party libraries through CocoaPods.
* Consumes a REST web service with JSON and uses asynchronous requests with Alamofire.
* Show an activity view with "KVNProgress" while the info is loading.
* iOS 8+
* Swift 3.0

### DEMO ###
* [Video](https://drive.google.com/file/d/0Bzb3k0CfykvDazJtS0RPaXpUVVU/view?usp=sharing)
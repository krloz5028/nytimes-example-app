//
//  DetailViewController.swift
//  TheNewYorkTimes
//
//  Created by Krloz on 28/08/16.
//  Copyright © 2016 CERR. All rights reserved.
//

import UIKit
import KVNProgress

class DetailViewController: UIViewController,UIWebViewDelegate {
    //MARK: Vars and IBOutlets
    @IBOutlet var webView: UIWebView!
    var urlString:String?
    
    //MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if urlString != nil {
            let  url = NSURL(string: urlString!)
            let request = NSURLRequest(URL: url!)
            KVNProgress.showWithStatus("Loading...")
            webView.loadRequest(request)
        }
        
    }
    
    //MARK: UIWebViewDelegate
    func webViewDidFinishLoad(webView: UIWebView) {
        KVNProgress.dismiss()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print(error?.localizedDescription)
    }
}

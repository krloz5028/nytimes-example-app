//
//  ListViewController.swift
//  TheNewYorkTimes
//
//  Created by Krloz on 27/08/16.
//  Copyright © 2016 CERR. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import KVNProgress

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    //MARK: Vars and IBOulets
    let CellIdentifier = "NewsCell"
    let dateformatter = NSDateFormatter()
    var dataSourceArray = []
    var images = [String:UIImage]()
    
    
    @IBOutlet var tableView: UITableView!
    
    //MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named:"nav_bar_logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        dateformatter.dateFormat = "dd/MM/yyyy"
        
        tableView.estimatedRowHeight = 300.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DetailSegue" {
            let vc = segue.destinationViewController as! DetailViewController
            vc.urlString = sender as? String
        }
    }
    
    func reloadData(){
        KVNProgress.showWithStatus("Loading...")
        NewsFeedService.getNews { (success, data, error) in
            if error == nil{
                self.dataSourceArray = data!.valueForKey("results") as! NSArray
                dispatch_async(dispatch_get_main_queue(), { 
                    self.tableView.reloadData()
                    KVNProgress.dismiss()
                })
            }else{
                KVNProgress.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK: Button actions
    
    @IBAction func reloadDataButtonTapped(sender: AnyObject) {
        reloadData()
    }
    
    //MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath) as! NewsTableViewCell
        
        let element = dataSourceArray[indexPath.row] as! [String:AnyObject!]
        
        cell.newsTitleLabel.text = element["title"] as? String
        cell.authorLabel.text = element["byline"] as? String
        cell.newsDetailsLabel.text = element["abstract"] as? String
        cell.dateLabel.text = element["published_date"] as? String
        cell.photoImageView.image = nil
        
        if let image = images["\(indexPath.row)"] {
            cell.photoImageView.image = image
        }else{
            if let media = element["media"]  {
                if media.count > 0 {
                    let media_metadata = media[0]["media-metadata"]
                    
                    var imageURL = ""
                    for item in media_metadata as! [[String:AnyObject!]]{
                        imageURL = item["url"] as! String
                    }
                    Alamofire.request(.GET, imageURL)
                        .responseImage { response in
                            if let image = response.result.value {
                                
                                self.images["\(indexPath.row)"] = image
                                cell.photoImageView.image = image
                            }
                    }
                }
            }
        }
        
        return cell ?? UITableViewCell()
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let element = dataSourceArray[indexPath.row] as! [String:AnyObject!]
        
        self.performSegueWithIdentifier("DetailSegue", sender: element["url"])
    }
}

//MARK: - NewsTableViewCell

class NewsTableViewCell: UITableViewCell{
    //MARK: Vars and IBOulets
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var newsTitleLabel: UILabel!
    @IBOutlet var newsDetailsLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
}


//
//  NewsFeedService.swift
//  TheNewYorkTimes
//
//  Created by Krloz on 27/08/16.
//  Copyright © 2016 CERR. All rights reserved.
//

import UIKit

class NewsFeedService: NSObject {
     static func getNews(completion:((success:Bool, data: NSDictionary?, error:NSError?) -> Void)?) {
        let urlPath: String = BASE_URL + "/mostshared/all-sections/30.json?api-key=" + API_KEY
        let url: NSURL = NSURL(string:urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        
        NSURLConnection.sendAsynchronousRequest(request,
                                                queue: NSOperationQueue(),
                                                completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            do {
                if let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                    completion!(success: true, data: jsonResult, error: nil)
                }else{
                    completion!(success: false, data: nil, error: error)
                }
            } catch let error as NSError {
                completion!(success: false, data: nil, error: error)
            }
        })
    }
}
